from dexml.fields import Value
import datetime
import logging


class Datetime(Value):
    """Field representing a datetime value."""
    def parse_value(self, val):
        return datetime.datetime.strptime(val, '%Y-%m-%d %H:%M:%S.%f')


class Logging:
    separator_str = '\n\n*********************************************************************************\n\n'

    @staticmethod
    def info(msg, separator=True):
        if separator:
            msg = Logging.separator_str + msg + Logging.separator_str

        logging.info(msg)
        print msg