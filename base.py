import dexml


class BaseApiModel(dexml.Model):
    def __init__(self, **kwargs):
        super(BaseApiModel, self).__init__(**kwargs)

    def as_xml(self, exclude=None):
        if not exclude:
            exclude = []

        fields = map(lambda f: f.tagname, filter(lambda f: hasattr(f, 'tagname') and f.tagname not in exclude, self._fields))
        serialized = []
        current_model = ''
        result = u''
        skip = False
        for r in self.irender(fragment=True):
            if self.__class__.__name__+'>' in r:
                continue

            tag = r[1:r.index(' />' if ' />' in r else '>')]
            if tag in exclude:
                continue

            if tag not in fields:
                if not skip:
                    r = getattr(self, tag.lower()).as_xml() + '\n'
                    result += r
                    current_model = tag
                    skip = True
                elif tag[1:] == current_model:
                    current_model = ''
                    skip = False
                    continue

            if skip:
                continue

            index = fields.index(tag)
            if index > 0:
                while serialized[-1] != fields[index - 1]:
                    empty_tag = fields[fields.index(serialized[-1])+1]
                    serialized.append(empty_tag)
                    result += '<'+empty_tag+' />\n'

            serialized.append(tag)
            result += r + u'\n'
        return result

    @staticmethod
    def parse_result(result):
        parsed = {}
        for k, v in result.iteritems():
            if isinstance(v, basestring):
                v = v.lower()
            elif isinstance(v, dict):
                v = BaseApiModel.parse_result(v)
            elif isinstance(v, list):
                v = map(lambda x: BaseApiModel.parse_result(x), v)
            parsed[k.lower()] = v
        return parsed